<?php
class Product_model extends CI_Model{
    public $id_product;
    public $nama_product;
    public $tersedia;
    public $image_url;
    public $create_at;
    public $delete_at;

    public function getProduct()
    {
        $this->load->database();
        $products = $this->db->get("produk");
        $result = $products->result();
        return json_encode ($result);
    }
}